$(document).ready(function(){
    /*var encoded = morsify.encode('SOS'); // ... --- ...
    alert (encoded)
        var decoded = morsify.decode('... --- ...'); // SOS
        var characters = morsify.characters(); // {'1': {'A': '.-', ...}, ..., '11': {'ã„±': '.-..', ...}}
        var audio = morsify.audio('SOS');
        var oscillator = audio.oscillator; // OscillatorNode
        var context = audio.context; // AudioContext
        var gainNode = audio.gainNode; // GainNode
    */

// find elements
    var banner = $("#banner-message")
    var buttonTextToSpace = $(".TextToSpace")
    var buttonSpaceToText = $(".SpaceToText")

// handle click and add class
    buttonSpaceToText.on("click", function() {
        var decoded = $('.inputSpace').val().replace(/â €/g, "-"); //   /_/g, "-"

        decoded = decoded.replace(/ã…¤/g, ".");

        decoded = decoded.replace(/ /g, " ");


        decoded = morsify.decode(decoded); //  ... --- ... -> SOS

        decoded = decoded.replace(/\+/g, " ");  //add space

        $('.inputText').val(decoded);



    })

    buttonTextToSpace.on("click", function() {

        var SpaceRemoved = $('.inputText').val()

        SpaceRemoved = SpaceRemoved.replace(/ /g, "+"); //   /_/g, "-"

        var encoded = morsify.encode(SpaceRemoved); // ... --- ...  convert text  first


        encoded = encoded.replace(/-/g, "â €"); //   /_/g, "-"

        encoded = encoded.replace(/\./g, "ã…¤");

        encoded = encoded.replace(/ /g, " ")

        $('.inputSpace').val(encoded)



    })
});